package com.codingtaskassured.test;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

class HomeControllerTest {

	private final String CONTEXT_PATH = "/CodingTask/";

	@BeforeEach
	void setUp() throws Exception {

		RestAssured.baseURI = "http://localhost";
		RestAssured.port = 8080;
	}

	@Test
	void testHomePage() {
		Response response = given().when().get(CONTEXT_PATH).then()
				.statusCode(200).contentType(ContentType.HTML).extract().response();
		
		
		assertTrue(response.getStatusCode() == 200);

	}

}
