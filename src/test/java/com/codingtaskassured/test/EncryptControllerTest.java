package com.codingtaskassured.test;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

class EncryptControllerTest {

	private final String CONTEXT_PATH = "/CodingTask/encrypt";

	@BeforeEach
	public void setUp() throws Exception {

		RestAssured.baseURI = "http://localhost";
		RestAssured.port = 8080;
	}

	@Test
	public void testResponse() throws JsonProcessingException {

		List<Map<String, Object>> listOfParams = new LinkedList<Map<String, Object>>();

		Map<String, Object> param1 = new HashMap<String, Object>();
		param1.put("script", "amirreza");
		param1.put("offset", "1");

		Map<String, Object> param2 = new HashMap<String, Object>();
		param2.put("script", "testtest test test");
		param2.put("offset", "9");

		listOfParams.add(param1);
		listOfParams.add(param2);

		Response response = given().contentType(ContentType.JSON).accept(ContentType.JSON).body(listOfParams.get(0))
				.when().post(CONTEXT_PATH).then().statusCode(200).contentType(ContentType.JSON).extract().response();

		String result = response.asString();

		assertTrue(result.equals("bnjssfab"));

		response = given().contentType(ContentType.JSON).accept(ContentType.JSON).body(listOfParams.get(1)).when()
				.post(CONTEXT_PATH).then().statusCode(200).contentType(ContentType.JSON).extract().response();

		assertTrue(response.asString().equals("cnbccnbc cnbc cnbc"));

	}

	@Test
	public void testErrorResponse() {

		Map<String, Object> badParam = new HashMap<String, Object>();

		badParam.put("script", " ");
		badParam.put("offset", 1);

		Response errorResponse = given().contentType(ContentType.JSON).accept(ContentType.JSON).body(badParam).when()
				.post(CONTEXT_PATH).then().statusCode(406).contentType(ContentType.JSON).extract().response();

		assertTrue(errorResponse.getStatusCode() == 406);
		assertTrue(errorResponse.asString().toLowerCase().contains("can not perform"));

	}

}
