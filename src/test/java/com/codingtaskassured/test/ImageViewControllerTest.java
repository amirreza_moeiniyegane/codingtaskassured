package com.codingtaskassured.test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

class ImageViewControllerTest {

	private final String CONTEXT_PATH = "CodingTask/images/cipher-disk.jpg";

	@BeforeEach
	void setUp() throws Exception {
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = 8080;
	}

	@Test
	void test() {

		Response response = given().when().get(CONTEXT_PATH).then().statusCode(200).contentType("image/jpeg")
				.extract().response();
		
		assertTrue(response.getStatusCode() == 200);

	}

}
